/*
 * # Data logger infotype example
 *
 * ## Global variable
 * d3: D3.js instance at version 3.0.0
 *
 * ## Mock data
 * random DATASOURCE: 90 data have two params `country` and `value`
 *  [{
 *    "country": 'EN',
 *    "value": 1
 *  },{
 *    "country": 'JP',
 *    "value": 4
 *  },
 *  ...]
 *
**/

DataLogger.defaultSettings = {
  "columnA" : "country",
  "columnB": "value"
};

/*
 * The schema of settings infomotion user can customize
 * - type Schema type `key`, `list`, `select`, `radio`, `checkbox` etc
 * - name The name of schema, this name used in the `settings` param.
 * - help The help desciption for infomotion user.
**/
DataLogger.settings = EnebularIntelligence.SchemaProcessor([
  {
    type : "key",
    name : "columnA",
    help : "A table column."
  },{
    type : "key",
    name : "columnB",
    help : "Another table column."
  }
], DataLogger.defaultSettings);

function DataLogger(settings, options) {
  /*
   * Sets up a infotype.
   *
   * @param {object} settings The settings customized by user.
   *    settings = {
   *      "SchemaProcessor array's name one": "a key",
   *      "SchemaProcessor array's name two": "another key"
   *      ...
   *    }
   * @param {object} options The optional values like `width`, `height`.
   *
  **/

  var self = this;

  this.settings = settings;
  this.options = options;
  this.el  = window.document.createElement('div');

  d3.select(this.el)
    .attr('class', 'Data-Logger');

  var table =  d3.select(this.el)
    .append('table')
    .attr('class', 'Data-Logger__table');

  appendTableHead(table);

  table
    .append('tbody')
    .attr('id', 'tbody');

  function appendTableHead (table) {
    var tr = table
      .append('thead')
        .append('tr');
    tr
      .append('th')
      .text(self.settings.columnA)
    tr
      .append('th')
      .text(self.settings.columnB)
  }
}

DataLogger.prototype.onDrillDown = function(cb) {
  /*
   * # onDrillDown API
   *
   * Trigger when drillDown by EnebularIntelligence
   *
   * @param {function} cb Some description.
   *
  **/
  this.onDrillDownListener = cb;
}

DataLogger.prototype.addData = function(data) {
  /*
   * # addData API
   *
   * Trigger when data added/updated by EnebularIntelligence
   * - Initial (after clearData)
   * - DatePicker updated (after clearData)
   * - Timeline updated (after clearData)
   * - Filter updated (after clearData)
   *
   * @param {array} data Added/Updated data
   *
  **/

  this.tbodyWithData = d3.select('#tbody')
    .selectAll('tr')
    .data(data.reverse());

  this.render()
}

DataLogger.prototype.render = function() {
  /*
   * This is not API, created just for readable code.
  **/
  var self = this;

  self.tbodyWithData.exit().remove();
  self.tbodyWithData.enter()
    .append('tr')
    .html(function (datum) {
      return '<td>' +
        (datum[self.settings.columnA] === undefined || datum[self.settings.columnA] === null ? 'No data' : datum[self.settings.columnA]) +
      '</td>' +
      '<td>' +
        (datum[self.settings.columnB] === undefined || datum[self.settings.columnB] === null ? 'No data' : datum[self.settings.columnB]) +
       '</td>';
    })
}

DataLogger.prototype.clearData = function() {
  /*
   * # clearData API
   *
   * Trigger when data clear by EnebularIntelligence
   * - Initial (before addData)
   * - DatePicker updated (before addData)
   * - Timeline updated (before addData)
   * - Filter updated (before addData)
   *
  **/
  this.tbodyWithData = d3.select('#tbody')
    .selectAll('tr')
    .data([]);

  this.render();
}

DataLogger.prototype.resize = function(options) {
  /*
   * # resize API
   *
   * Trigger when graph canvas resized by EnebularIntelligence
   *
   * @param {object} options The optional values like `width`, `height`.
   *
  **/
  this.height = options.height;
  this.width  = options.width;
}

DataLogger.prototype.getEl = function() {
  /*
   * # getEl API
   *
   * Reference the root element of this graph.
   *
  **/
  return this.el;
}

/*
 * Register to use in infomotion
**/
EnebularIntelligence.register('Data-Logger', DataLogger);

module.exports = DataLogger;